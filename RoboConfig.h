// konfigurace parametru servo-motoru pro ovladani a prevod pro jednotlive servo-motory robo-ruky

#ifndef RoboConfig_h
#define RoboConfig_h

#include "RoboServo.h"

// rameno otoceni
const PWMServo PWM_SERVO_ARM_ROTATION = {
  0, // channel
  -90, // min, left
  +90, // max, right
  0, // reset
  0, // pwmOn
  510, // pwmOffMin, left
  // 300, center
  100 // pwmOffMax, right
};

// rameno zvedani
const PWMServo PWM_SERVO_ARM_LIFT = {
  1, // channel
  -90, // min, down
  +90, // max, up
  0, // reset
  0, // pwmOn
  115, // pwmOffMin, down
  // 256, center
  406 // pwmOffMax, up
};

// loket zvedani
const PWMServo PWM_SERVO_ELBOW_LIFT = {
  2, // channel
  -135, // min, down
  +135, // max, up
  0, // reset
  0, // pwmOn
  533, // pwmOffMax, diagonal down, -135
  // 458, horizontal down, -90
  // 308, center
  // 165, horizontal up, +90
  90 // pwmOffMin, diagonal up, +135
};

// zapesti zvedani
const PWMServo PWM_SERVO_WRIST_LIFT = {
  3, // channel
  -90, // min, down
  +90, // max, up
  0, // reset
  0, // pwmOn
  510, // pwmOffMin, down
  // 300, center
  100 // pwmOffMax, up
};

// zapesti otoceni
const PWMServo PWM_SERVO_WRIST_ROTATION = {
  4, // channel
  -45, // min, right-of-horizontal
  135, // max, right-of-vertical
  0, // reset
  0, // pwmOn
  90, // pwmOffMin, right-of-horizontal
  // 195, horizontal
  // 300, left-of-horizontal = right-of-vertical
  // 405, vertical
  510 // pwmOffMax, right-of-vertical
};

// celisti
const PWMServo PWM_SERVO_JAWS = {
  5, // channel
  0, // min, closed
  55, // max, opened
  0, // reset
  0, // pwmOn
  380, // pwmOffMin, closed
  230 // pwmOffMax, opened
};

#endif

// trida pro robo-ruku

#include "Robo.h"
#include "RoboServo.h"
#include "RoboConfig.h"

// https://www.arduino.cc/en/Reference/Wire
//#include <Wire.h>

// https://learn.adafruit.com/16-channel-pwm-servo-driver/library-reference
#include <Adafruit_PWMServoDriver.h>


Robo::Robo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver, const PWMServo &pwmServoArmRotation, const PWMServo &pwmServoArmLift,
           const PWMServo &pwmServoElbowLift, const PWMServo &pwmServoWristLift, const PWMServo &pwmServoWristRotation, const PWMServo &pwmServoJaws)
  : adafruitPWMServoDriver(&adafruitPWMServoDriver),
    armRotation(new RoboServo(adafruitPWMServoDriver, pwmServoArmRotation)),
    armLift(new RoboServo(adafruitPWMServoDriver, pwmServoArmLift)),
    elbowLift(new RoboServo(adafruitPWMServoDriver, pwmServoElbowLift)),
    wristLift(new RoboServo(adafruitPWMServoDriver, pwmServoWristLift)),
    wristRotation(new RoboServo(adafruitPWMServoDriver, pwmServoWristRotation)),
    jaws(new RoboServo(adafruitPWMServoDriver, pwmServoJaws)) { }

Robo::Robo(const PWMServo &pwmServoArmRotation, const PWMServo &pwmServoArmLift, const PWMServo &pwmServoElbowLift,
           const PWMServo &pwmServoWristLift, const PWMServo &pwmServoWristRotation, const PWMServo &pwmServoJaws)
  : adafruitPWMServoDriver(new Adafruit_PWMServoDriver()),
    armRotation(new RoboServo(adafruitPWMServoDriver, pwmServoArmRotation)),
    armLift(new RoboServo(adafruitPWMServoDriver, pwmServoArmLift)),
    elbowLift(new RoboServo(adafruitPWMServoDriver, pwmServoElbowLift)),
    wristLift(new RoboServo(adafruitPWMServoDriver, pwmServoWristLift)),
    wristRotation(new RoboServo(adafruitPWMServoDriver, pwmServoWristRotation)),
    jaws(new RoboServo(adafruitPWMServoDriver, pwmServoJaws)) { }

Robo::Robo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver)
  : adafruitPWMServoDriver(&adafruitPWMServoDriver),
    armRotation(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_ARM_ROTATION)),
    armLift(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_ARM_LIFT)),
    elbowLift(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_ELBOW_LIFT)),
    wristLift(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_WRIST_LIFT)),
    wristRotation(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_WRIST_ROTATION)),
    jaws(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_JAWS)) { }

Robo::Robo()
  : adafruitPWMServoDriver(new Adafruit_PWMServoDriver()),
    armRotation(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_ARM_ROTATION)),
    armLift(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_ARM_LIFT)),
    elbowLift(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_ELBOW_LIFT)),
    wristLift(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_WRIST_LIFT)),
    wristRotation(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_WRIST_ROTATION)),
    jaws(new RoboServo(adafruitPWMServoDriver, PWM_SERVO_JAWS)) { }

void Robo::begin() {
  this->adafruitPWMServoDriver->begin();
  this->adafruitPWMServoDriver->setPWMFreq(50);
}

void Robo::reset() {
  this->armRotation->reset();
  this->armLift->reset();
  this->elbowLift->reset();
  this->wristLift->reset();
  this->wristRotation->reset();
  this->jaws->reset();
}

void Robo::set() {
  this->armRotation->set();
  this->armLift->set();
  this->elbowLift->set();
  this->wristLift->set();
  this->wristRotation->set();
  this->jaws->set();
}

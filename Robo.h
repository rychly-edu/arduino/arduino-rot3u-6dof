// trida pro robo-ruku

#ifndef Robo_h
#define Robo_h

#include "RoboServo.h"

// https://www.arduino.cc/en/Reference/Wire
//#include <Wire.h>

// https://learn.adafruit.com/16-channel-pwm-servo-driver/library-reference
#include <Adafruit_PWMServoDriver.h>


class Robo {

  public:

    /* attrs */

    const Adafruit_PWMServoDriver *adafruitPWMServoDriver;

    // rameno otoceni
    const RoboServo *armRotation;

    // rameno zvedani
    const RoboServo *armLift;

    // loket zvedani
    const RoboServo *elbowLift;

    // zapesti zvedani
    const RoboServo *wristLift;

    // zapesti otoceni
    const RoboServo *wristRotation;

    // celisti
    const RoboServo *jaws;

    /* methods */

    // konstruktory
    Robo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver, const PWMServo &pwmServoArmRotation, const PWMServo &pwmServoArmLift,
         const PWMServo &pwmServoElbowLift, const PWMServo &pwmServoWristLift, const PWMServo &pwmServoWristRotation, const PWMServo &pwmServoJaws);
    Robo(const PWMServo &pwmServoArmRotation, const PWMServo &pwmServoArmLift, const PWMServo &pwmServoElbowLift,
         const PWMServo &pwmServoWristLift, const PWMServo &pwmServoWristRotation, const PWMServo &pwmServoJaws);
    Robo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver);
    Robo();

    // inicializace ovladace
    void begin();

    // nastaveni cele robo-ruky do vychozi polohy
    void reset();

    // nastaveni cele robo-ruky do aktualni polohy (opakovane volane udrzuje ruku v aktualni poloze)
    void set();

};

#endif

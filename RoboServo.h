// trida pro predkonfigurovany servo-motor robo-ruky

#ifndef RoboServo_h
#define RoboServo_h

// https://www.arduino.cc/en/Reference/Wire
//#include <Wire.h>

// https://learn.adafruit.com/16-channel-pwm-servo-driver/library-reference
#include <Adafruit_PWMServoDriver.h>


// datovy typ pro uzivatelskou hodnotu pohybu servo-motoru
typedef int16_t PWMServoValue;

// datovy typ pro konfiguraci parametru servo-motoru pro ovladani a prevod z uzivatelske hodnoty na PWM hodnotu
typedef struct {
  uint8_t channel;
  PWMServoValue min, max, reset;
  uint16_t pwmOn, pwmOffMin, pwmOffMax;
} PWMServo;

// pohyb servo-motoru robo-ruky dle jeho konfigurace a pozadovane uzivatelske hodnoty
bool movePWMServo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver, const PWMServo &pwmServo, const PWMServoValue pwmServoValue);

// test pohybu servo-motoru robo-ruky dle jeho konfigurace v celem rozsahu uzivatelskych hodnot
void testPWMServo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver, const PWMServo &pwmServo, const uint16_t delayMs = 25);

// trida pro predkonfigurovany servo-motor robo-ruky
class RoboServo {
  private:
    /* attrs */
    PWMServoValue _value;
  public:

    /* attrs */

    const Adafruit_PWMServoDriver *adafruitPWMServoDriver;
    const PWMServo *pwmServo;

    /* methods */

    // konstruktor
    RoboServo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver, const PWMServo &pwmServo);

    // nastavi servo-motor na aktualni polohu
    bool set();

    // nastavi servo-motor na danou uzivatelskou polohu
    bool set(const PWMServoValue value);

    // snizi uzivatelskou polohu o decrement a nastavi na ne servo-motor
    bool dec(const PWMServoValue decrement);

    // zvysi uzivatelskou polohu o increment a nastavi na ne servo-motor
    bool inc(const PWMServoValue increment);

    // provede testovaci pohyby servo-motoru a pote vrati zpet do predchozi polohy
    void test(const uint16_t delayMs = 25);

    // nastavi servo-motor do vychozi polohy
    void reset();

    // nastavi servo-motor do minimalni polohy
    void setMin();

    // nastavi servo-motor do maximalni polohy
    void setMax();

    // vrati hodnotu aktualni uzivatelske polohy
    PWMServoValue get();

    // vrati hodnotu vychozi uzivatelske polohy
    PWMServoValue getReset();

    // vrati hodnotu minimalni uzivatelske polohy
    PWMServoValue getMin();

    // vrati hodnotu maximalni uzivatelske polohy
    PWMServoValue getMax();

};

#endif

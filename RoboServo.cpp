// trida pro predkonfigurovany servo-motor robo-ruky

#include "RoboServo.h"

// https://www.arduino.cc/en/Reference/Wire
//#include <Wire.h>

// https://learn.adafruit.com/16-channel-pwm-servo-driver/library-reference
#include <Adafruit_PWMServoDriver.h>


// pohyb servo-motoru robo-ruky dle jeho konfigurace a pozadovane uzivatelske hodnoty
bool movePWMServo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver, const PWMServo &pwmServo, const PWMServoValue pwmServoValue) {
  // zkontroluje, jestli je hodnota v povolenem rozmezi; pokud neni, tak skonci a vrat nepravdu
  if ((pwmServoValue < pwmServo.min) || (pwmServoValue > pwmServo.max)) return false;
  // preved hodnotu na udaj pro servo-motor
  const uint16_t pwmOff = map(pwmServoValue, pwmServo.min, pwmServo.max, pwmServo.pwmOffMin, pwmServo.pwmOffMax);
  // pohni servo-motorem
  adafruitPWMServoDriver.setPWM(pwmServo.channel, pwmServo.pwmOn, pwmOff);
  // skonci a vrat pravdu
  return true;
}

// test pohybu servo-motoru robo-ruky dle jeho konfigurace v celem rozsahu uzivatelskych hodnot
void testPWMServo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver, const PWMServo &pwmServo, const uint16_t delayMs = 25) {
  // zjisti mame-li pri pohybu prvnim smerem pricitat nebo odcitat
  const int8_t increment = pwmServo.min <= pwmServo.max ? +1 : -1;
  // pohyb prvnim smerem
  for (PWMServoValue i = pwmServo.min; (i >= pwmServo.min) && (i <= pwmServo.max); i += increment) {
    movePWMServo(adafruitPWMServoDriver, pwmServo, i);
    delay(delayMs);
  }
  delay(delayMs * 2);
  // pohyb druhym smerem
  for (PWMServoValue i = pwmServo.max; (i >= pwmServo.min) && (i <= pwmServo.max); i -= increment) {
    movePWMServo(adafruitPWMServoDriver, pwmServo, i);
    delay(delayMs);
  }
  delay(delayMs * 2);
}


RoboServo::RoboServo(const Adafruit_PWMServoDriver &adafruitPWMServoDriver, const PWMServo &pwmServo)
  : adafruitPWMServoDriver(&adafruitPWMServoDriver),
    pwmServo(&pwmServo),
    _value(pwmServo.reset) { }

bool RoboServo::set() {
  return this->set(this->_value);
}

bool RoboServo::set(const PWMServoValue value) {
  if (movePWMServo(*this->adafruitPWMServoDriver, *this->pwmServo, value)) {
    this->_value = value;
    return true;
  }
  return false;
}

bool RoboServo::dec(const PWMServoValue decrement) {
  return this->set(this->_value - decrement);
}

bool RoboServo::inc(const PWMServoValue increment) {
  return this->set(this->_value + increment);
}

void RoboServo::test(const uint16_t delayMs = 25) {
  testPWMServo(*this->adafruitPWMServoDriver, *this->pwmServo, delayMs);
  this->set();
}

void RoboServo::reset() {
  this->set(this->pwmServo->reset);
}

void RoboServo::setMin() {
  this->set(this->pwmServo->min);
}

void RoboServo::setMax() {
  this->set(this->pwmServo->max);
}

PWMServoValue RoboServo::get() {
  return this->_value;
}

PWMServoValue RoboServo::getReset() {
  return this->pwmServo->reset;
}

PWMServoValue RoboServo::getMin() {
  return this->pwmServo->min;
}

PWMServoValue RoboServo::getMax() {
  return this->pwmServo->max;
}

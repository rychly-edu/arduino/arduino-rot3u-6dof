#include "Robo.h"

// https://learn.adafruit.com/16-channel-pwm-servo-driver/library-reference
#include <Adafruit_PWMServoDriver.h>

const Adafruit_PWMServoDriver pwmsd;
const Robo robo(pwmsd);

#include <PsxControllerBitBang.h>

const byte PIN_PS2_DAT = 9; // 1 fialova
const byte PIN_PS2_CMD = 10; // 2 modra
// GND = 4 cerna
// VCC 3.3V = 5 fialova
const byte PIN_PS2_ATT = 11;  // 6 modra
const byte PIN_PS2_CLK = 12; // 7 cerna

PsxControllerBitBang<PIN_PS2_ATT, PIN_PS2_CMD, PIN_PS2_DAT, PIN_PS2_CLK> psx;

const unsigned long DOBA_CEKANI = 1000U / 50U;
unsigned long posledni_odezva = 0;

boolean ovladac_pripraven = false;

void aktivuj_ovladac() {
  if (psx.begin()) {
    Serial.println("Nalezen ovladac!");
    if (!psx.enterConfigMode()) {
      Serial.println("Ovladac nelze prepnout do nastavovaciho modu!");
      return;
    } else {
      if (!psx.enableAnalogSticks()) {
        Serial.println("Nelze aktivovat analogove paky ovladace!");
        return;
      }
      if (!psx.exitConfigMode()) {
        Serial.println("Nelze ukoncit nastavovaci mod ovladace!");
        return;
      }
    }
    Serial.println("Ovladac deaktivovan!");
    ovladac_pripraven = true;
  }
}

void deaktivuj_ovladac() {
  Serial.println("Ovladac deaktivovan!");
  ovladac_pripraven = false;
}

void setup() {
  Serial.begin(115200);
  // zapnuti robo-ruky
  robo.begin();
  robo.reset();
}

void loop() {
  // zjisti, jestli mame zareagovat
  if (millis () - posledni_odezva >= DOBA_CEKANI) {
    posledni_odezva = millis ();
    // zkontroluj, by-li ovladac pripraven
    if (!ovladac_pripraven) {
      // aktivuj dosud nepripraveny ovladac
      aktivuj_ovladac();
    } else {
      // pokus se precist stav ovladace
      if (!psx.read()) {
        // deaktivuj ovladac, pokud selhalo cteni (byl odpojen)
        deaktivuj_ovladac();
      } else {
        // paky
        byte x, y;
        if (psx.getLeftAnalog(x, y)) {
          // prevod prectenych hodnot na rozsah
          short rx = map(x, 0, 255, -2, 2);
          short ry = map(y, 0, 255, -2, 2);
          // otaceni ramene
          robo.armRotation->inc(rx);
          // zvedani ramene
          robo.armLift->inc(ry);
        }
        if (psx.getRightAnalog(x, y)) {
          // prevod prectenych hodnot na rozsah
          short rx = map(x, 0, 255, -2, 2);
          short ry = map(y, 0, 255, -2, 2);
          // otaceni ramene
          robo.armRotation->inc(rx);
          // zvedani lokte
          robo.elbowLift->inc(ry);
        }
        // sipky
        if (psx.buttonPressed(PSB_PAD_UP)) {
          // zvedani zapesti
          robo.wristLift->inc(+1);
        }
        if (psx.buttonPressed(PSB_PAD_DOWN)) {
          // klesani zapesti
          robo.wristLift->inc(-1);
        }
        if (psx.buttonPressed(PSB_PAD_LEFT)) {
          // otaceni zapesti doleva
          robo.wristRotation->inc(-1);
        }
        if (psx.buttonPressed(PSB_PAD_RIGHT)) {
          // otaceni zapesti doprava
          robo.wristRotation->inc(+1);
        }
        // predni tlacitka
        if (psx.buttonPressed(PSB_L2)) {
          // rozevreni celisti
          robo.jaws->inc(+1);
        }
        if (psx.buttonPressed(PSB_R2)) {
          // sevreni celisti
          robo.jaws->inc(-1);
        }
        // tlacitka ostatni
        if (psx.buttonPressed(PSB_START)) {
          // reset cela ruka
          robo.reset();
        }
        if (psx.buttonPressed(PSB_SELECT)) {
          // reset ruka bez celisti
          robo.armRotation->reset();
          robo.armLift->reset();
          robo.elbowLift->reset();
          robo.wristLift->reset();
          robo.wristRotation->reset();
        }
      }
    }
  }
}

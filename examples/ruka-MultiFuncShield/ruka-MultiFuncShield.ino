#include <Robo.h>

// https://learn.adafruit.com/16-channel-pwm-servo-driver/library-reference
#include <Adafruit_PWMServoDriver.h>

const Adafruit_PWMServoDriver pwmsd;
const Robo robo(pwmsd);

const byte AXIS_X = A0;
const byte AXIS_Y = A1;
//const byte UP_BTN = 2; // vypnuto kvuli I2C na Leonardo
const byte DOWN_BTN = 4;
const byte LEFT_BTN = 5;
//const byte RIGHT_BTN = 3; // vypnuto kvuli I2C na Leonardo
const byte E_BTN = 6;
const byte F_BTN = 7;
const byte JOY_BTN = 8;

// stavy ovladani, mezi kterymi lze prepinat
const byte RAMENO = 1;
const byte LOKET = 2;
const byte ZAPESTI = 3;
const byte PRSTY = 4;

// aktualni stav ovladani
byte ovladani = RAMENO;

void setup() {
  pinMode(AXIS_X, INPUT);
  pinMode(AXIS_Y, INPUT);
  //pinMode(UP_BTN, INPUT_PULLUP); // vypnuto kvuli I2C na Leonardo
  pinMode(DOWN_BTN, INPUT_PULLUP);
  pinMode(LEFT_BTN, INPUT_PULLUP);
  //pinMode(RIGHT_BTN, INPUT_PULLUP); // vypnuto kvuli I2C na Leonardo
  pinMode(E_BTN, INPUT_PULLUP);
  pinMode(F_BTN, INPUT_PULLUP);
  pinMode(JOY_BTN, INPUT_PULLUP);
  // zapnuti robo-ruky
  robo.begin();
  robo.reset();
}

void loop() {
  jerab();
//  if (dioda) {
  
//  }
}

void jerab() {
  // cteni joysticku s opravou osy X
  int ax = analogRead(AXIS_X) + 20;
  int ay = analogRead(AXIS_Y);
  // prevod prectenych hodnot na rozsah -10 az +10
  short x = map(ax, 0, 1023, -10, 10);
  short y = map(ay, 0, 1023, 10, -10);

  // cteni stavu tlacitek
  bool ovladani_dalsi = digitalRead(LEFT_BTN) == LOW;
  bool ovladani_predchozi = digitalRead(DOWN_BTN) == LOW;
  bool ruka_reset_krome_prstu = digitalRead(F_BTN) == LOW;
  bool ruka_reset_cela = digitalRead(E_BTN) == LOW;

  if (ruka_reset_krome_prstu) {
    robo.armLift->reset();
    robo.elbowLift->reset();
    robo.wristLift->reset();
    robo.wristRotation->reset();
  }

  if (ruka_reset_cela) {
    robo.reset();
  }

  switch (ovladani) {
    case RAMENO: {
        robo.armRotation->inc(x);
        robo.armLift->inc(y);
        if (ovladani_dalsi) {
          ovladani = LOKET;
        }
        break;
      }
    case LOKET: {
        robo.armRotation->inc(x);
        robo.elbowLift->inc(y);
        if (ovladani_dalsi) {
          ovladani = ZAPESTI;
        }
        if (ovladani_predchozi) {
          ovladani = RAMENO;
        }
        break;
      }
    case ZAPESTI: {
        robo.wristRotation->inc(x);
        robo.wristLift->inc(y);
        if (ovladani_dalsi) {
          ovladani = PRSTY;
        }
        if (ovladani_predchozi) {
          ovladani = LOKET;
        }
        break;
      }
    case PRSTY: {
        robo.jaws->inc(x);
        robo.wristLift->inc(y);
        if (ovladani_predchozi) {
          ovladani = ZAPESTI;
        }
        break;
      }
  }

  delay(100);
}

void ukazka() {
  robo.armRotation->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.armLift->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.elbowLift->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.wristLift->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.wristRotation->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.jaws->test(25);
  delay(500);


  // ukazka prace s jednim motorem

  robo.armRotation->setMax();
  delay(1000);
  robo.armRotation->setMin();
  delay(1000);

  for (int i = robo.armRotation->getMin(); i < robo.armRotation->getMax(); i++) {
    robo.armRotation->set(i);
    delay(25);
  }

  robo.armRotation->reset();
  delay(1000);
}

void klokan() {
  // ruka rovne
  delay(1000);
  robo.reset();
  delay(1000);

  // test jednotlivych motoru

  robo.armLift->set(-45);
  robo.elbowLift->set(-65);
  robo.wristLift->set(-30);
  robo.wristRotation->set(90);
  robo.jaws->setMax();
  delay(1000);
  robo.jaws->set(10);
  delay(3000);

  robo.armLift->reset();
  robo.elbowLift->reset();
  robo.wristLift->reset();
  robo.wristRotation->reset();
  delay(6000);

  robo.armLift->set(-45);
  robo.elbowLift->set(-65);
  robo.wristLift->set(-30);
  robo.wristRotation->set(90);
  delay(1000);
  robo.jaws->setMax();
  delay(3000);

  robo.armLift->reset();
  robo.elbowLift->reset();
  robo.wristLift->reset();
  robo.wristRotation->reset();
}

#include <Robo.h>

const Robo robo;

void setup() {
  robo.begin();
}

void loop() {

  digitalWrite(13, HIGH);
  delay(250);
  digitalWrite(13, LOW);
  delay(250);


  // ruka rovne
  robo.reset();


  // test jednotlivych motoru

  robo.armRotation->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.armLift->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.elbowLift->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.wristLift->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.wristRotation->test(25);
  delay(500);

  // vraceni vsech motoru do nastaveneho stavu, pokud se mezitim nejak pohnuly
  robo.set();

  robo.jaws->test(25);
  delay(500);


  // ukazka prace s jednim motorem

  robo.armRotation->setMax();
  delay(1000);
  robo.armRotation->setMin();
  delay(1000);

  for (int i = robo.armRotation->getMin(); i < robo.armRotation->getMax(); i++) {
    robo.armRotation->set(i);
    delay(25);
  }

  robo.armRotation->reset();
  delay(1000);

}
